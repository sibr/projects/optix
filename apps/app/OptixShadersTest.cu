/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <optix_device.h>

#include "projects/optix/renderer/OptixSharedStructures.h"

using namespace sibr;

namespace sibr {
	
	extern "C" __global__ void __closesthit__radiance()
	{
		const TriangleMeshData &sbtData = *(const TriangleMeshData*)optixGetSbtDataPointer();
		const int   primID = optixGetPrimitiveIndex();

		const sibr::Vector3u index = sbtData.index[primID];
		const float u = optixGetTriangleBarycentrics().x;
		const float v = optixGetTriangleBarycentrics().y;
		
		sibr::Vector3f n = (1.0f - u - v) * sbtData.normal[index[0]]
				+ u * sbtData.normal[index[1]]
				+ v * sbtData.normal[index[2]];
		n.normalize();

		const sibr::Vector3f diffuseColor = (1.0f - u - v) * sbtData.color[index[0]]
			+ u * sbtData.color[index[1]]
			+ v * sbtData.color[index[2]];
		
		// Arbitrary light direction in object space.
		const sibr::Vector3f l0 = sibr::Vector3f(0.5f, 0.8f, 0.2f).normalized();

		const float cosDN = 0.2f + 0.8f*max(l0.dot(n), 0.0f);
		
		sibr::Vector3f &prd = *(sibr::Vector3f*)getPRD<sibr::Vector3f>();
		prd = cosDN * diffuseColor;
		
	}
	
	extern "C" __global__ void __closesthit__pos()
	{
		const TriangleMeshData &sbtData = *(const TriangleMeshData*)optixGetSbtDataPointer();
		const int   primID = optixGetPrimitiveIndex();

		const sibr::Vector3u index = sbtData.index[primID];
		const float u = optixGetTriangleBarycentrics().x;
		const float v = optixGetTriangleBarycentrics().y;
		
		sibr::Vector3f pos = (1.0f - u - v) * sbtData.vertex[index[0]]
				+ u * sbtData.vertex[index[1]]
				+ v * sbtData.vertex[index[2]];
		
		sibr::Vector3f &prd = *(sibr::Vector3f*)getPRD<sibr::Vector3f>();
		prd = pos;
		
	}



	extern "C" __global__ void __anyhit__radiance()
	{ /*! for this simple example, this will remain empty */
	}

	extern "C" __global__ void __miss__radiance()
	{
		sibr::Vector3f &prd = *(sibr::Vector3f*)getPRD<sibr::Vector3f>();
		// set to constant white as background color
		prd = {1.0f,1.0f, 1.0f};
	}

	extern "C" __global__ void __raygen__renderFrameFromCamera()
	{
		// compute a test pattern based on pixel ID
		const int ix = optixGetLaunchIndex().x;
		const int iy = optixGetLaunchIndex().y;

		const auto &camera = optixLaunchParams.camera;
		sibr::Vector3f pixelColorPRD = { 0.f, 0.0f, 0.0f };

		// Pack the pointer to the result color in the payload.
		uint32_t u0, u1;
		packPointer(&pixelColorPRD, u0, u1);

		// Compute pixel coordinates, flipping to be in the same orientation as OpenGL.
		sibr::Vector2f screen(float(ix) + 0.5f, optixLaunchParams.size[1] - float(iy) - 0.5f);
		 
		// generate ray direction
		sibr::Vector3f worldPos = camera.upLeftOffset + screen[0] * camera.dx + screen[1] * camera.dy;
		sibr::Vector3f rayDir = (worldPos - camera.position).normalized();
		

		float3 cudaPos = {camera.position[0], camera.position[1], camera.position[2]};
		float3 cudaDir = { rayDir[0], rayDir[1], rayDir[2] };
		optixTrace(optixLaunchParams.traversable,
			cudaPos, cudaDir, optixLaunchParams.minDist, 1e20f, 0.0f,
			OptixVisibilityMask(255), OPTIX_RAY_FLAG_DISABLE_ANYHIT,
			SURFACE_RAY_TYPE, RAY_TYPE_COUNT, SURFACE_RAY_TYPE /* missSBTIndex */, u0, u1);

		const sibr::Vector3i rgb = (255.99f * pixelColorPRD).cast<int>();

		// Convert to 32-bit rgba value.
		const uint32_t rgba = 0xff000000 | ((rgb[0] & 0xff) << 0) | ((rgb[1] & 0xff) << 8) | ((rgb[2] & 0xff) << 16);


		// and write to frame buffer ...
		const uint32_t fbIndex = ix + iy * optixLaunchParams.size[0];
		optixLaunchParams.dst[fbIndex] = rgba;
	}

	extern "C" __global__ void __raygen__renderFrameFromBuffer()
	{
		// compute a test pattern based on pixel ID
		const int ix = optixGetLaunchIndex().x;
		const int iy = optixGetLaunchIndex().y;

		sibr::Vector3f pixelColorPRD = { 0.f, 0.0f, 0.0f };

		// Pack the pointer to the result color in the payload.
		uint32_t u0, u1;
		packPointer(&pixelColorPRD, u0, u1);

		// Compute pixel coordinates, flipping to be in the same orientation as OpenGL.
		sibr::Vector2f screen(float(ix) + 0.5f, optixLaunchParams.size[1] - float(iy) - 0.5f);
		const uint32_t fbIndex = ix + iy * optixLaunchParams.size[0];

		// generate ray direction
		const float4 value = tex2D<float4>(optixLaunchParams.buffers.positions, ix, iy);
		const float4 valueDir = tex2D<float4>(optixLaunchParams.buffers.directions, ix, iy);
		float3 cudaPos = { value.x, value.y, value.z };
		float3 cudaDir = { valueDir.x, valueDir.y, valueDir.z };
		optixTrace(optixLaunchParams.traversable,
			cudaPos, cudaDir, optixLaunchParams.minDist, 1e20f, 0.0f,
			OptixVisibilityMask(255), OPTIX_RAY_FLAG_DISABLE_ANYHIT,
			SURFACE_RAY_TYPE, RAY_TYPE_COUNT, SURFACE_RAY_TYPE /* missSBTIndex */, u0, u1);
		
		const sibr::Vector3ub rgb = (255.9f * pixelColorPRD).cast<unsigned char>();

		// Convert to 32-bit rgba value.
		const uint32_t rgba = 0xff000000 | ((rgb[0] & 0xff) << 0) | ((rgb[1] & 0xff) << 8) | ((rgb[2] & 0xff) << 16);

		// and write to frame buffer ...
		
		optixLaunchParams.dst[fbIndex] = rgba;
	}
	
	}
