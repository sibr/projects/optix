/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include <projects/optix/renderer/OptixRaycaster.hpp>

#include <core/graphics/Window.hpp>
#include <core/graphics/Input.hpp>
#include <core/graphics/Utils.hpp>
#include <core/system/String.hpp>
#include <core/assets/Resources.hpp>
#include <core/view/InteractiveCameraHandler.hpp>
#include <core/renderer/ColoredMeshRenderer.hpp>

#include <imgui/imgui.h>

#define PROGRAM_NAME "sibr_optix_demo_app"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;



extern "C" char embedded_ptx_code_test[];


int main(int ac, char** av) {

	CommandLineArgs::parseMainArgs(ac, av);
	BasicIBRAppArgs args;
	
	Window window(1280,800, "Optix Demo", args);
	window.makeContextCurrent();
	sibr::Mesh::Ptr mesh(new Mesh());
	mesh->load(std::string(args.dataset_path) + "/mesh.obj");
	mesh->generateNormals();
	if (!mesh->hasColors()) {
		const size_t vCount = mesh->vertices().size();
		std::vector<sibr::Vector3f> colors(vCount);
		for (int vid = 0; vid < vCount; ++vid) {
			colors[vid] = sibr::jetColor<float>(float(vid)/(vCount-1));
		}
		mesh->colors(colors);
	}
	InteractiveCameraHandler handler;
	handler.setup(mesh, window.viewport());
	SIBR_LOG << "OpenGL setup." << std::endl;
	CHECK_GL_ERROR;
	
	RenderTargetRGBA::Ptr dst(new RenderTargetRGBA(window.w(), window.h()));
	RenderTargetRGBA::Ptr dstOGL(new RenderTargetRGBA(window.w(), window.h()));
	RenderTargetRGBA32F::Ptr positions(new RenderTargetRGBA32F(window.w(), window.h(), 0, 2));

	ColoredMeshRenderer rendererOGL;
	GLShader posShader;
	GLuniform<sibr::Matrix4f> mvp;
	GLuniform<sibr::Vector3f> camPos;
	posShader.init("positionRenderer",
		sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("positionReflectedDirRenderer.vert")),
		sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("positionReflectedDirRenderer.frag")));
	mvp.init(posShader, "MVP");
	camPos.init(posShader, "cameraPos");


	// For spawning rays from positions, define USE_BUFFERS.
#define USE_BUFFERS
	
	OptixRaycaster renderer;
	SIBR_LOG << "Creating module and programs." << std::endl;

	OptixRaycaster::Options options;
	options.missName = "__miss__radiance";
	options.closestName = "__closesthit__radiance";
	options.anyName = "__anyhit__radiance";
	options.ptxCode = std::string(embedded_ptx_code_test);
	options.debug = true;
	
#ifdef USE_BUFFERS
	options.entryName = "__raygen__renderFrameFromBuffer";
#else
	options.entryName = "__raygen__renderFrameFromCamera";
#endif
	
	renderer.createPrograms(options);
	
	SIBR_LOG << "Adding mesh." << std::endl;
	renderer.createScene(*mesh);

	SIBR_LOG << "Binding program records." << std::endl;
	renderer.bindProgramsAndScene();

#ifdef USE_BUFFERS
	// For spawning rays from positions.
	renderer.registerSource(*positions, 0);
	renderer.registerDirection(*positions, 1);
#endif
	renderer.registerDestination(dst);

	CHECK_GL_ERROR;
	SIBR_LOG << "Setup complete." << std::endl;
	bool showDebug = false;

	while (window.isOpened()) {
		sibr::Input::poll();
		window.makeContextCurrent();
		if (sibr::Input::global().key().isPressed(sibr::Key::Escape)) {
			window.close();
		}
		if(ImGui::Begin("Optix Demo")) {
			ImGui::Checkbox("Show debug OpenGL rendering", &showDebug);
			ImGui::Text("%.3f ms (%.1f fps)", ImGui::GetIO().DeltaTime*1000.0f, 1.0f/ImGui::GetIO().DeltaTime);
		}
		ImGui::End();
		
		handler.update(sibr::Input::global(), 1.0f/60.0f, window.viewport());

	
		window.viewport().bind();
		
#ifdef USE_BUFFERS
		// Render positions and directions:
		positions->clear();
		positions->bind();
		posShader.begin();
		mvp.set(handler.getCamera().viewproj());
		camPos.set(handler.getCamera().position());
		mesh->render(true, true);

		posShader.end();
		positions->unbind();

		// For spawning rays from positions.
		renderer.onRender();
#else
		// Using a camera to spawn rays.
		renderer.onRender(handler.getCamera());
#endif
		
		sibr::blit(*dst, window);

		if (showDebug) {
			rendererOGL.process(*mesh, handler.getCamera(), *dstOGL, sibr::Mesh::FillRenderMode, false);
			sibr::blit(*dstOGL, window);
		}
		handler.onRender(window.viewport());

		CHECK_GL_ERROR;
		window.swapBuffer();
		CHECK_GL_ERROR;
	}
	SIBR_LOG << "Rendering." << std::endl;
	
	return EXIT_SUCCESS;
}