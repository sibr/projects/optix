/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include <projects/optix/renderer/OptixDenoiser.hpp>

#include <core/graphics/Window.hpp>
#include <core/graphics/Input.hpp>
#include <core/graphics/Utils.hpp>
#include <core/system/String.hpp>
#include <core/view/InteractiveCameraHandler.hpp>
#include <core/renderer/ColoredMeshRenderer.hpp>

#define PROGRAM_NAME "sibr_optix_denoiser"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;


struct OptixDenoisingAppArgs :
	virtual AppArgs {
	RequiredArg<std::string> path = { "path", "path to the images directory" };
	Arg<std::string> extension = { "type", "png", "images extension" };
	Arg<std::string> output = { "output", "", "output directory path" };
	Arg<std::string> albedoPath = { "albedoPath", "", "Albedo directory path, containing files with the same name as in path with png extension" };
};


int main(int ac, char** av) {

	sibr::CommandLineArgs::parseMainArgs(ac, av);
	OptixDenoisingAppArgs args;

	// Remove the extension dot if it exists.
	std::string extension = args.extension;
	if (!extension.empty() && extension[0] == '.') {
		extension = extension.substr(1);
	}
	const bool isHDR = extension == "exr" || extension == "hdr";
	// Input/output paths.
	const std::string inputPath = args.path;
	const std::string albedoPath = args.albedoPath;
	std::string outputPath = args.output;
	std::string outputSuff = "." + extension;
	// If we output in the same dir, we want to avoid collisions.
	if (outputPath.empty()) {
		outputPath = inputPath;
		outputSuff = "_denoised" + outputSuff;
	}
	else {
		sibr::makeDirectory(outputPath);
	}

	const auto files = sibr::listFiles(inputPath, false, false, { extension });
	Denoiser denoiser;

	bool useAlbedo = albedoPath != "";

	for (const auto& file : files) {
		const std::string src = inputPath + "/" + file;
		const std::string dst = outputPath + "/" + sibr::removeExtension(file) + outputSuff;

		sibr::ImageRGB32F::Ptr albedoImg(new sibr::ImageRGB32F());
		if (useAlbedo) {
			const std::string srcAlbedo = albedoPath + "/" + sibr::removeExtension(file) + ".png";
			albedoImg->load(srcAlbedo);
		}
		else {
			albedoImg = nullptr;
		}

		if (isHDR) {
			sibr::ImageRGB32F hdrImg;
			hdrImg.load(src);
			sibr::ImageRGB32F::Ptr res;
			res = denoiser.denoise(hdrImg, albedoImg);
			res->saveHDR(dst);
		}
		else {
			sibr::ImageRGB ldrImg;
			ldrImg.load(src);
			sibr::ImageRGB::Ptr res = denoiser.denoise(ldrImg);
			res->save(dst);
		}
	}

	return EXIT_SUCCESS;
}