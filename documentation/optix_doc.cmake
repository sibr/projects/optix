# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


set(PROJECT_PAGE "optixPage")
set(PROJECT_LINK "https://gitlab.inria.fr/sibr/projects/optix")
set(PROJECT_DESCRIPTION "SIBR/OptiX integration example")
set(PROJECT_TYPE "TOOLBOX")