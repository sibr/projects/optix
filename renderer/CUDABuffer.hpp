/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include "Config.hpp"
#include <cuda_runtime.h>
#include <optix.h>
#include <cassert>
#include <vector>

namespace sibr {

	/**
	Wrapper around a CUDA buffer to simplify alloc/update/cleanup. Based on Ingo Wald's Optix 7 tutorial.
	*/
	struct CUDABuffer {
	public:

		/** Allocate a non-initialized buffer.
		 *\param size the size in bytes to allocate
		 */
		void alloc(size_t size) {
			assert(_devicePtr == nullptr);
			_size = size;
			CUDA_CHECK(cudaMalloc((void**)&_devicePtr, _size));
		}

		/** Free the buffer. */
		void free() {
			CUDA_CHECK(cudaFree(_devicePtr));
			_devicePtr = nullptr;
			_size = 0;
		}

		/** Resize the buffer, reseting its content.
		 *\param size the size in bytes to allocate
		 */
		void resize(size_t size) {
			if (_devicePtr) {
				free();
			}
			alloc(size);
		}

		/** Send data to the buffer.
		 * \param t a pointer to an array of elements to store
		 * \param count elements count
		 */
		template<typename T>
		void send(const T *t, size_t count) {
			assert(_devicePtr != nullptr);
			assert(_size == count * sizeof(T));
			CUDA_CHECK(cudaMemcpy(_devicePtr, (void *)t,
				count * sizeof(T), cudaMemcpyHostToDevice));
		}

		/** Read data from the buffer.
		 * \param t a pointer to a preallocated array that will contain a copy of the buffer content
		 * \param count elements count to read
		 */
		template<typename T>
		void readBack(T *t, size_t count){
			assert(_devicePtr != nullptr);
			assert(_size == count * sizeof(T));
			CUDA_CHECK(cudaMemcpy((void *)t, _devicePtr,
				count * sizeof(T), cudaMemcpyDeviceToHost));
		}

		/** Allocate and send data to the buffer.
		 *\param vt a list of elements to store.
		 */
		template<typename T>
		void set(const std::vector<T> &vt) {
			alloc(vt.size() * sizeof(T));
			send((const T*)vt.data(), vt.size());
		}

		/** \return the buffer pointer on the GPU. */
		inline CUdeviceptr devicePtr() const {
			return (CUdeviceptr)_devicePtr;
		}

		/** \return the buffer size in bytes. */
		inline size_t size() const {
			return _size;
		}

		/** \return true if the buffer is allocated. */
		operator bool() const {
			return _devicePtr !=  nullptr;
		}

	private:

		size_t _size = 0; ///< Buffer size in bytes.
		void* _devicePtr = nullptr; ///< Buffer pointer.
	};

}