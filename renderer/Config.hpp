/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_OPTIX_CONFIG_HPP__
# define __SIBR_OPTIX_CONFIG_HPP__

# include <core/system/Config.hpp>

# ifdef SIBR_OS_WINDOWS
#  ifdef SIBR_STATIC_DEFINE
#    define SIBR_EXPORT
#    define SIBR_NO_EXPORT
#  else
#    ifndef SIBR_OPTIX_EXPORT
#      ifdef SIBR_OPTIX_EXPORTS
/* We are building this library */
#        define SIBR_OPTIX_EXPORT __declspec(dllexport)
#      else
/* We are using this library */
#        define SIBR_OPTIX_EXPORT __declspec(dllimport)
#      endif
#    endif
#    ifndef SIBR_NO_EXPORT
#      define SIBR_NO_EXPORT
#    endif
#  endif
# else
#  define SIBR_EXP_ULR_EXPORT
# endif


// For now the CUDA/OPTIX checks are here, there is probably some overlap with the ones in tfgl_interop.

#define CUDA_CHECK(call)												\
    {																	\
      cudaError_t rc = call;                                            \
      if (rc != cudaSuccess) {                                          \
        std::stringstream txt;                                          \
        cudaError_t err =  rc; /*cudaGetLastError();*/                  \
        SIBR_ERR << "[CUDA] Error " << cudaGetErrorName(err)            \
            << " (" << cudaGetErrorString(err) << ")" << std::endl;     \
      }                                                                 \
    }

#define CUDA_SYNC_CHECK()                                               \
  {                                                                     \
    cudaDeviceSynchronize();                                            \
    cudaError_t error = cudaGetLastError();                             \
    if( error != cudaSuccess ){                                         \
        SIBR_ERR << "[CUDA] Error: " << cudaGetErrorString( error ) << std::endl; \
    }                                                                 \
  }

#define OPTIX_CHECK( call )                                             \
  {                                                                     \
    OptixResult res = call;                                             \
    if( res != OPTIX_SUCCESS ) {                                        \
		SIBR_ERR << "[OPTIX] Error with code: " << int(res) << std::endl;\
    }                                                                   \
  }





#endif  //__SIBR_OPTIX_CONFIG_HPP__
