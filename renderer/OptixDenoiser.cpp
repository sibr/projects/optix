/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "OptixDenoiser.hpp"
#include <core/raycaster/CameraRaycaster.hpp>
#include <core/system/SimpleTimer.hpp>
// This include should be present in only one translation unit.

using namespace sibr;

namespace sibr {



	static void optixLogCallback(unsigned int level, const char *tag, const char *message, void *) {
		SIBR_LOG << "[Optix][" << level << "][" << tag << "] " << message << std::endl;
	}

	Denoiser::Denoiser() {
		// Get a CUDA device and context.
		cudaFree(0);
		int deviceCount;
		cudaGetDeviceCount(&deviceCount);
		if (deviceCount == 0) {
			SIBR_WRG << "[Optix] Unable to find a CUDA device." << std::endl;
			return;
		}
		OPTIX_CHECK(optixInit());
		const int deviceID = 0;
		CUDA_CHECK(cudaSetDevice(deviceID));
		CUDA_CHECK(cudaStreamCreate(&_cudaStream));
		cudaGetDeviceProperties(&_deviceProps, deviceID);
		SIBR_LOG << "[Optix] Using device " << _deviceProps.name << std::endl;


		CUresult contextStat = cuCtxGetCurrent(&_cudaContext);
		if (contextStat != CUDA_SUCCESS) {
			SIBR_ERR << "[Optix] Unable to query the current context." << std::endl;
			return;
		}

		OPTIX_CHECK(optixDeviceContextCreate(_cudaContext, nullptr, &_optixContext));
		// Set a callback for log, the last int determines the verbosity level.
		OPTIX_CHECK(optixDeviceContextSetLogCallback(_optixContext, optixLogCallback, nullptr, 4));

	}

	void Denoiser::init(const sibr::Vector2u& size, bool hdr, bool albedo) {
		// If it's already allocated, de-init.
		if(_denoiser) {
			CUDA_SYNC_CHECK();
			OPTIX_CHECK(optixDenoiserDestroy(_denoiser));
			_scratch.free();
			_state.free();
			CUDA_SYNC_CHECK();
			_denoiser = nullptr;
		}
		
		OptixDenoiserOptions options;
		if (albedo) {
			options.inputKind = OPTIX_DENOISER_INPUT_RGB_ALBEDO;
		}
		else {
		options.inputKind = OPTIX_DENOISER_INPUT_RGB;
		}
		options.pixelFormat = OPTIX_PIXEL_FORMAT_FLOAT3;
		OPTIX_CHECK(optixDenoiserCreate(_optixContext, &options, &_denoiser));
		OPTIX_CHECK(optixDenoiserSetModel(_denoiser, hdr ? OPTIX_DENOISER_MODEL_KIND_HDR : OPTIX_DENOISER_MODEL_KIND_LDR, nullptr, 0));
		OptixDenoiserSizes sizes;
		OPTIX_CHECK(optixDenoiserComputeMemoryResources(_denoiser, size[0], size[1], &sizes));
		_state.alloc(sizes.stateSizeInBytes);
		_scratch.alloc(sizes.recommendedScratchSizeInBytes);

		OPTIX_CHECK(optixDenoiserSetup(_denoiser, _cudaStream, size[0], size[1],
			_state.devicePtr(), _state.size(), _scratch.devicePtr(), _scratch.size()));
		SIBR_LOG << "[Denoiser] Denoiser initialized." << std::endl;
		_size = size;
		_isHdr = hdr;
		_useAlbedo = albedo;
	}

	Denoiser::~Denoiser() {
		CUDA_SYNC_CHECK();
		if (_denoiser) {
			OPTIX_CHECK(optixDenoiserDestroy(_denoiser));
		}
		if (_scratch) {
			_scratch.free();
		}
		if (_state) {
			_state.free();
		}
		CUDA_SYNC_CHECK();
	}

	sibr::ImageRGB32F::Ptr Denoiser::denoise(const sibr::ImageRGB32F& hdr, const sibr::ImageRGB32F::Ptr& albedo) {
		if (hdr.w() != _size[0] || hdr.h() != _size[1] || !_isHdr || (bool)albedo != _useAlbedo) {
			SIBR_WRG << "[Denoiser] Image size or range is different from denoiser current settings. Re-initializing first, which has a performance cost..." << std::endl;
			// Else re-setup the denoiser at the right resolution.
			init(hdr.size(), true, albedo.operator bool());
		}
		int ch = 3;
		if (_useAlbedo) {
			std::cout << "Using albedo" << std::endl;
			ch = 6;
		}
		std::vector<float> inputVec(hdr.w() * hdr.h() * ch);
#pragma omp parallel for 
		for (int y = 0; y < int(hdr.h()); ++y) {
			for (int x = 0; x < int(hdr.w()); ++x) {
				const size_t bid = 3*(x + y * hdr.w());
				inputVec[bid + 0] = hdr(x,y)[0];
				inputVec[bid + 1] = hdr(x, y)[1];
				inputVec[bid + 2] = hdr(x, y)[2];
			}
		}

		if (_useAlbedo) {
#pragma omp parallel for 
			for (int y = 0; y < int(hdr.h()); ++y) {
				for (int x = 0; x < int(hdr.w()); ++x) {
					const size_t bid = 3 * (x + y * hdr.w()) + hdr.w() * hdr.h() * 3;
					inputVec[bid + 0] = albedo(x, y)[0];
					inputVec[bid + 1] = albedo(x, y)[1];
					inputVec[bid + 2] = albedo(x, y)[2];
				}
			}
		}

		std::vector<float> outputVec;

		process(hdr.w(), hdr.h(), ch, inputVec, outputVec);

		ImageRGB32F::Ptr resImg(new ImageRGB32F(hdr.w(), hdr.h()));
#pragma omp parallel for
		for (int y = 0; y < int(hdr.h()); ++y) {
			for (int x = 0; x < int(hdr.w()); ++x) {
				const size_t bid = 3 * (x + y * hdr.w());
				sibr::Vector3f resCol;
				resCol[0] = outputVec[bid + 0];
				resCol[1] = outputVec[bid + 1];
				resCol[2] = outputVec[bid + 2];
				resImg(x,y) = resCol;
			}
		}
		return resImg;
	}

	sibr::ImageRGB::Ptr Denoiser::denoise(const sibr::ImageRGB & ldr) {
		if (ldr.w() != _size[0] || ldr.h() != _size[1] || _isHdr) {
			SIBR_WRG << "[Denoiser] Image size or range is different from denoiser current settings. Re-initializing first, which has a performance cost..." << std::endl;
			init(ldr.size(), false);
		}
		std::vector<float> inputVec(ldr.w()*ldr.h() * 3);
#pragma omp parallel for 
		for (int y = 0; y < int(ldr.h()); ++y) {
			for (int x = 0; x < int(ldr.w()); ++x) {
				const size_t bid = 3 * (x + y * ldr.w());
				inputVec[bid + 0] = float(ldr(x, y)[0])/255.0f;
				inputVec[bid + 1] = float(ldr(x, y)[1])/255.0f;
				inputVec[bid + 2] = float(ldr(x, y)[2])/255.0f;
			}
		}
		
		std::vector<float> outputVec;
		process(ldr.w(), ldr.h(), 3, inputVec, outputVec);

		ImageRGB::Ptr resImg(new ImageRGB(ldr.w(), ldr.h()));
#pragma omp parallel for
		for (int y = 0; y < int(ldr.h()); ++y) {
			for (int x = 0; x < int(ldr.w()); ++x) {
				const size_t bid = 3 * (x + y * ldr.w());
				sibr::Vector3f resCol;
				resCol[0] = outputVec[bid + 0];
				resCol[1] = outputVec[bid + 1];
				resCol[2] = outputVec[bid + 2];
				resCol = sibr::clamp((255.f*resCol).eval(), { 0.0f, 0.0f, 0.0f }, {255.0f, 255.0f, 255.0f});
				resImg(x, y) = resCol.cast<uchar>();
			}
		}
		return resImg;
	}

	void Denoiser::process(unsigned int w, unsigned int h, unsigned int components, const std::vector<float> & inputData, std::vector<float> & outputData) const {
		const size_t compsCount = w * h*components;
		SIBR_ASSERT(compsCount == inputData.size());
		if (_useAlbedo) {
			SIBR_ASSERT(components == 6);
		}
		else {
		SIBR_ASSERT(components == 3 || components == 4);
		}
		const OptixPixelFormat commonFormat = components == 4 ? OPTIX_PIXEL_FORMAT_FLOAT4 : OPTIX_PIXEL_FORMAT_FLOAT3;
		const unsigned pixSize = unsigned(commonFormat == OPTIX_PIXEL_FORMAT_FLOAT4 ? sizeof(float4) : sizeof(float3));
		CUDABuffer inputBuffer;
		inputBuffer.set(inputData);
		CUDABuffer outputBuffer;
		int outCompsCount = components == 4 ? w * h * 4 : w * h * 3;
		outputBuffer.alloc(outCompsCount * sizeof(float));

		OptixImage2D input[3]= {};
		for (int c = 0; c < 3; c++) {
			input[c].width = w;
			input[c].height = h;
			input[c].pixelStrideInBytes = pixSize;
			input[c].rowStrideInBytes = w * pixSize;
			input[c].format = commonFormat;
			input[c].data = inputBuffer.devicePtr() + 3*c*(w*h)*sizeof(float);
		}

		OptixImage2D output;
		output.width = w;
		output.height = h;
		output.pixelStrideInBytes = pixSize;
		output.rowStrideInBytes = w * pixSize;
		output.format = commonFormat;
		output.data = outputBuffer.devicePtr();

		SIBR_LOG << "[Denoiser] Denoising...";
		CUDABuffer intensity;
		intensity.alloc(sizeof(float));
		OptixDenoiserParams params;
		params.denoiseAlpha = false;
		params.blendFactor = 0.0f;
		params.hdrIntensity = intensity.devicePtr();
		
		OPTIX_CHECK(optixDenoiserComputeIntensity(_denoiser, _cudaStream,
			&input[0], intensity.devicePtr(), _scratch.devicePtr(), _scratch.size()));

		int numIn = 1;
		if (_useAlbedo) {
			numIn = 2;
		}
		OPTIX_CHECK(optixDenoiserInvoke(_denoiser, _cudaStream, &params,
			_state.devicePtr(), _state.size(),
			input, numIn, 0, 0, &output,
			_scratch.devicePtr(), _scratch.size()));
		CUDA_SYNC_CHECK();
		std::cout << "Done." << std::endl;

		outputData.resize(outCompsCount);
		outputBuffer.readBack(outputData.data(), outCompsCount);

		outputBuffer.free();
		inputBuffer.free();
		intensity.free();

		inputBuffer.free();
		outputBuffer.free();
		intensity.free();
		
	}
}