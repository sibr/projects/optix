/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include "Config.hpp"
#include "CUDABuffer.hpp"

#include <core/graphics/Mesh.hpp>
#include <core/graphics/RenderTarget.hpp>
#include <core/assets/InputCamera.hpp>
#include <core/system/Matrix.hpp>

#include <optix.h>
# pragma warning(push, 0)
#include <optix_stubs.h>
# pragma warning(pop)
#include <cuda_gl_interop.h>

namespace sibr
{
	/** \brief The Denoiser provide a wrapper over Optix 7 denoiser.
	 * It can be used to denoise raytraced LDR and HDR images, for instance generated with Mitsuba.
	 * The image should be as "raw" as possible, meaning no reconstruction filter or other samples post-processing should be applied (apart from clamping).
	 * In Mitsuba, you will want to use a reconstruction box filter with radius 0.5.
	 * See the Optix7 documentation (https://raytracing-docs.nvidia.com/optix7/guide/index.html#ai_denoiser#nvidia-ai-denoiser) for additional details.
	 */
	class SIBR_OPTIX_EXPORT Denoiser
	{
	public:

		SIBR_DISALLOW_COPY(Denoiser);
		SIBR_CLASS_PTR(Denoiser);

		/** Initialize CUDA and Optix contexts. */
		Denoiser();

		/** Deinitialize the internal denoiser. */
		~Denoiser();

		/** Denoise a HDR image (values should be between 0.0 and 10000.0, non-tonemaped, non-gamma-corrected).
		Adjusting intensity and clamping fireflies might help, according to Optix documentation.
		\param hdr the image to denoise
		\param albedo an optional pointer to an albedo rendering corresponding to the image to denoise
		\return a pointer to the denoised image
		*/

		sibr::ImageRGB32F::Ptr denoise(const sibr::ImageRGB32F & hdr, const sibr::ImageRGB32F::Ptr & albedo = nullptr);
		/** Denoise a LDR image (values should be tonemaped and with a 2.2 gamma).
		\param ldr the image to denoise
		\return a pointer to the denoised image
		*/
		sibr::ImageRGB::Ptr denoise(const sibr::ImageRGB & ldr);

	private:

		/** Initialize the denoiser for a given size and range.
		\param size the maximum image size
		\param hdr will the input be HDR images or LDR (\see denoise)
		\param albedo will an additional input image containing albedo be provided (\see denoise).
		
		*/
		void init(const sibr::Vector2u & size, bool hdr, bool albedo = false);

		/** Denoise a raw float image buffer.
		\param w the image width
		\param h the image height
		\param components number of components of the image
		\param inputData raw float input data (for LDR images, should be rescaled to 0.0,1.0)
		\param outputData raw float output data (for LDR images, will be in 0.0,1.0)
		*/
		void process(unsigned int w, unsigned int h, unsigned int components, const std::vector<float> & inputData, std::vector<float> & outputData) const;

		CUcontext _cudaContext = nullptr; ///< Cuda context.
		CUstream _cudaStream = nullptr; ///< Cuda command stream.
		cudaDeviceProp _deviceProps; ///< Cuda device parameters.
		OptixDeviceContext _optixContext = nullptr; ///< Optix context.
		OptixDenoiser _denoiser = nullptr; ///< Optix denoiser.
		CUDABuffer _state, _scratch;  ///< CUDA working memory for the denoiser.
		sibr::Vector2u _size = { 0, 0 }; ///< Current maximum image size.
		bool _isHdr = false; ///< Current range.
		bool _useAlbedo = false; ///< Use albedo to guide denoising.
	};


} // namespace sibr

