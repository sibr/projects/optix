/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "OptixRaycaster.hpp"
#include <core/raycaster/CameraRaycaster.hpp>
// This include should be present in only one translation unit.
#include "optix_function_table_definition.h"

using namespace sibr;

namespace sibr {


	static void optixLogCallback(unsigned int level, const char* tag, const char* message, void*) {
		SIBR_LOG << "[Optix][" << level << "][" << tag << "] " << message << std::endl;
	}

	/*! SBT record for a raygen program */
	struct __align__(OPTIX_SBT_RECORD_ALIGNMENT) RaygenRecord
	{
		__align__(OPTIX_SBT_RECORD_ALIGNMENT) char header[OPTIX_SBT_RECORD_HEADER_SIZE];
		// just a dummy value
		void* data;
	};

	/*! SBT record for a miss program */
	struct __align__(OPTIX_SBT_RECORD_ALIGNMENT) MissRecord
	{
		__align__(OPTIX_SBT_RECORD_ALIGNMENT) char header[OPTIX_SBT_RECORD_HEADER_SIZE];
		// just a dummy value
		void* data;
	};

	/*! SBT record for a hitgroup program */
	struct __align__(OPTIX_SBT_RECORD_ALIGNMENT) HitgroupRecord
	{
		__align__(OPTIX_SBT_RECORD_ALIGNMENT) char header[OPTIX_SBT_RECORD_HEADER_SIZE];
		TriangleMeshData data;
	};


	OptixRaycaster::OptixRaycaster() {

		// Get a CUDA device and context.
		cudaFree(0);
		int deviceCount;
		cudaGetDeviceCount(&deviceCount);
		if (deviceCount == 0) {
			SIBR_WRG << "[Optix] Unable to find a CUDA device." << std::endl;
			return;
		}
		OPTIX_CHECK(optixInit());
		const int deviceID = 0;
		CUDA_CHECK(cudaSetDevice(deviceID));
		CUDA_CHECK(cudaStreamCreate(&_cudaStream));
		cudaGetDeviceProperties(&_deviceProps, deviceID);
		SIBR_LOG << "[Optix] Using device " << _deviceProps.name << std::endl;


		CUresult contextStat = cuCtxGetCurrent(&_cudaContext);
		if (contextStat != CUDA_SUCCESS) {
			SIBR_ERR << "[Optix] Unable to query the current context." << std::endl;
			return;
		}

		OPTIX_CHECK(optixDeviceContextCreate(_cudaContext, 0, &_optixContext));
		// Set a callback for log, the last int determines the verbosity level.
		OPTIX_CHECK(optixDeviceContextSetLogCallback(_optixContext, optixLogCallback, nullptr, 4));
		_init = true;

		_launchParams.reset(new LaunchParams());
	}

	void OptixRaycaster::setMinDist(float minDist) {
		_launchParams->minDist = minDist;
	}


	void OptixRaycaster::createPrograms(Options options) {

		if (!_init) {
			SIBR_ERR << "[Optix] Optix is not initialized." << std::endl;
			return;
		}

		// Setup the module that will contain our programs.
		_moduleOptions.maxRegisterCount = 100;
		_moduleOptions.optLevel = options.debug ? OPTIX_COMPILE_OPTIMIZATION_LEVEL_0 : OPTIX_COMPILE_OPTIMIZATION_LEVEL_3;
		_moduleOptions.debugLevel = options.debug ? OPTIX_COMPILE_DEBUG_LEVEL_LINEINFO : OPTIX_COMPILE_DEBUG_LEVEL_NONE;

		_compileOptions = {};
		_compileOptions.traversableGraphFlags = OPTIX_TRAVERSABLE_GRAPH_FLAG_ALLOW_ANY;
		_compileOptions.usesMotionBlur = false;
		_compileOptions.numPayloadValues = 2;
		_compileOptions.numAttributeValues = 2;
		_compileOptions.exceptionFlags = OPTIX_EXCEPTION_FLAG_NONE;
		_compileOptions.pipelineLaunchParamsVariableName = "optixLaunchParams";

		_linkOptions.overrideUsesMotionBlur = false;
		_linkOptions.maxTraceDepth = 2;


		char log[2048];
		size_t logSize = sizeof(log);
		OPTIX_CHECK(optixModuleCreateFromPTX(_optixContext,
			&_moduleOptions,
			&_compileOptions,
			options.ptxCode.c_str(),
			options.ptxCode.size(),
			log, &logSize,
			&_module));

		if (logSize > 1) {
			SIBR_LOG << "[Optix] " << std::string(log) << std::endl;
		}

		{

			// we do a single ray gen program in this example:
			_raygens.resize(1);

			OptixProgramGroupOptions pgOptions = {};
			OptixProgramGroupDesc pgDesc = {};
			pgDesc.kind = OPTIX_PROGRAM_GROUP_KIND_RAYGEN;
			pgDesc.raygen.module = _module;
			// Pick the right spawn mode.
			pgDesc.raygen.entryFunctionName = options.entryName.c_str();

			// OptixProgramGroup raypg;
			char log[2048];
			size_t logSize = sizeof(log);
			OPTIX_CHECK(optixProgramGroupCreate(_optixContext,
				&pgDesc, 1, &pgOptions,
				log, &logSize,
				&_raygens[0]
			));
			if (logSize > 1) {
				SIBR_LOG << "[Optix] " << std::string(log) << std::endl;
			}
		}

		{
			_misses.resize(1);

			OptixProgramGroupOptions pgOptions = {};
			OptixProgramGroupDesc pgDesc = {};
			pgDesc.kind = OPTIX_PROGRAM_GROUP_KIND_MISS;
			pgDesc.miss.module = _module;
			pgDesc.miss.entryFunctionName = options.missName.c_str();

			// OptixProgramGroup raypg;
			char log[2048];
			size_t logSize = sizeof(log);
			OPTIX_CHECK(optixProgramGroupCreate(_optixContext,
				&pgDesc, 1, &pgOptions,
				log, &logSize,
				&_misses[0]
			));
			if (logSize > 1) {
				SIBR_LOG << "[Optix] " << std::string(log) << std::endl;
			}
		}
		{
			// for this simple example, we set up a single hit group
			_hitgroups.resize(1);

			OptixProgramGroupOptions pgOptions = {};
			OptixProgramGroupDesc pgDesc = {};
			pgDesc.kind = OPTIX_PROGRAM_GROUP_KIND_HITGROUP;
			pgDesc.hitgroup.moduleCH = _module;
			pgDesc.hitgroup.entryFunctionNameCH = options.closestName.c_str();

			pgDesc.hitgroup.moduleAH = _module;
			pgDesc.hitgroup.entryFunctionNameAH = options.anyName.c_str();

			char log[2048];
			size_t logSize = sizeof(log);
			OPTIX_CHECK(optixProgramGroupCreate(_optixContext,
				&pgDesc, 1, &pgOptions,
				log, &logSize,
				&_hitgroups[0]
			));
			if (logSize > 1) {
				SIBR_LOG << "[Optix] " << std::string(log) << std::endl;
			}
		}

		// Once all the programs are defined, we can create the pipeline.
		createPipeline();
	}

	void OptixRaycaster::createPipeline() {
		std::vector<OptixProgramGroup> programGroups;
		for (auto pg : _raygens) {
			programGroups.push_back(pg);
		}
		for (auto pg : _misses) {
			programGroups.push_back(pg);
		}
		for (auto pg : _hitgroups) {
			programGroups.push_back(pg);
		}

		char log[2048];
		size_t logSize = sizeof(log);
		OPTIX_CHECK(optixPipelineCreate(_optixContext,
			&_compileOptions,
			&_linkOptions,
			programGroups.data(),
			(int)programGroups.size(),
			log, &logSize,
			&_pipeline
		));
		if (logSize > 1) {
			SIBR_LOG << "[Optix] " << std::string(log) << std::endl;
		}

		OPTIX_CHECK(optixPipelineSetStackSize(_pipeline,
			/* [in] The direct stack size requirement for direct
			   callables invoked from IS or AH. */
			2 * 1024,
			/* [in] The direct stack size requirement for direct
			   callables invoked from RG, MS, or CH.  */
			2 * 1024,
			/* [in] The continuation stack requirement. */
			2 * 1024,
			/* [in] The maximum depth of a traversable graph
			   passed to trace. */
			3));
		if (logSize > 1) {
			SIBR_LOG << "[Optix] " << std::string(log) << std::endl;
		}
	}


	void OptixRaycaster::createScene(const sibr::Mesh& mesh) {
		if (!_init) {
			SIBR_ERR << "[Optix] Optix is not initialized." << std::endl;
			return;
		}

		_vertices.set(mesh.vertices());
		_indices.set(mesh.triangles());
		if (mesh.hasNormals()) {
			_normals.set(mesh.normals());
		}
		if (mesh.hasColors()) {
			_colors.set(mesh.colors());
		}
		if (mesh.hasTexCoords()) {
			_texcoords.set(mesh.texCoords());
		}

		OptixTraversableHandle handle = 0;

		// Create input data structure.
		OptixBuildInput triangleInput = {};
		triangleInput.type = OPTIX_BUILD_INPUT_TYPE_TRIANGLES;
		CUdeviceptr vPtr = _vertices.devicePtr();
		CUdeviceptr iPtr = _indices.devicePtr();

		triangleInput.triangleArray.vertexFormat = OPTIX_VERTEX_FORMAT_FLOAT3;
		triangleInput.triangleArray.vertexStrideInBytes = sizeof(sibr::Vector3f);
		triangleInput.triangleArray.numVertices = (int)mesh.vertices().size();
		triangleInput.triangleArray.vertexBuffers = &vPtr;
		triangleInput.triangleArray.indexFormat = OPTIX_INDICES_FORMAT_UNSIGNED_INT3;
		triangleInput.triangleArray.indexStrideInBytes = sizeof(sibr::Vector3u);
		triangleInput.triangleArray.numIndexTriplets = (int)mesh.triangles().size();
		triangleInput.triangleArray.indexBuffer = iPtr;

		uint32_t triangleInputFlags[1] = { 0 };
		triangleInput.triangleArray.flags = triangleInputFlags;
		triangleInput.triangleArray.numSbtRecords = 1;
		triangleInput.triangleArray.sbtIndexOffsetBuffer = 0;
		triangleInput.triangleArray.sbtIndexOffsetSizeInBytes = 0;
		triangleInput.triangleArray.sbtIndexOffsetStrideInBytes = 0;

		// Build options for OptiX.
		OptixAccelBuildOptions accelOptions = {};
		accelOptions.buildFlags = OPTIX_BUILD_FLAG_NONE | OPTIX_BUILD_FLAG_ALLOW_COMPACTION;
		accelOptions.motionOptions.numKeys = 1;
		accelOptions.operation = OPTIX_BUILD_OPERATION_BUILD;
		// Query the required sizes for the intermediate and final buffers.
		OptixAccelBufferSizes blasBufferSizes;
		OPTIX_CHECK(optixAccelComputeMemoryUsage(_optixContext,
			&accelOptions, &triangleInput, 1 /*num_build_inputs*/, &blasBufferSizes));
		// Query the compacted size.
		CUDABuffer compactedSizeBuffer;
		compactedSizeBuffer.alloc(sizeof(uint64_t));
		OptixAccelEmitDesc emitDesc;
		emitDesc.type = OPTIX_PROPERTY_TYPE_COMPACTED_SIZE;
		emitDesc.result = compactedSizeBuffer.devicePtr();

		// Build the acceleration structure.
		CUDABuffer tempBuffer;
		tempBuffer.alloc(blasBufferSizes.tempSizeInBytes);
		CUDABuffer outputBuffer;
		outputBuffer.alloc(blasBufferSizes.outputSizeInBytes);

		OPTIX_CHECK(optixAccelBuild(_optixContext, /*stream*/ 0,
			&accelOptions, &triangleInput, 1,
			tempBuffer.devicePtr(), tempBuffer.size(),
			outputBuffer.devicePtr(), outputBuffer.size(),
			&handle, &emitDesc, 1));
		CUDA_SYNC_CHECK();

		// Compaction.
		uint64_t compactedSize;
		compactedSizeBuffer.readBack(&compactedSize, 1);
		_ASBuffer.alloc(compactedSize);
		OPTIX_CHECK(optixAccelCompact(_optixContext, /*stream*/0,
			handle, _ASBuffer.devicePtr(), _ASBuffer.size(), &handle));
		CUDA_SYNC_CHECK();

		// Cleanup.
		outputBuffer.free();
		tempBuffer.free();
		compactedSizeBuffer.free();

		_launchParams->traversable = handle;
	}


	void OptixRaycaster::createScene(const MaterialMesh& mesh) {
		if (mesh.hasMatIds()) {
			_materials.set(mesh.matIds());
		}
		createScene(static_cast<Mesh>(mesh));
	}



	void OptixRaycaster::bindProgramsAndScene() {
		if (!_init) {
			SIBR_ERR << "[Optix] Optix is not initialized." << std::endl;
			return;
		}

		// Setup the STB.
		std::vector<RaygenRecord> raygenRecords;
		for (int i = 0; i < _raygens.size(); i++) {
			RaygenRecord rec;
			OPTIX_CHECK(optixSbtRecordPackHeader(_raygens[i], &rec));
			rec.data = nullptr;
			raygenRecords.push_back(rec);
		}
		_raygenRecords.set(raygenRecords);
		_sbt.raygenRecord = _raygenRecords.devicePtr();

		std::vector<MissRecord> missRecords;
		for (int i = 0; i < _misses.size(); i++) {
			MissRecord rec;
			OPTIX_CHECK(optixSbtRecordPackHeader(_misses[i], &rec));
			rec.data = nullptr;
			missRecords.push_back(rec);
		}
		_missRecords.set(missRecords);
		_sbt.missRecordBase = _missRecords.devicePtr();
		_sbt.missRecordStrideInBytes = sizeof(MissRecord);
		_sbt.missRecordCount = (int)missRecords.size();

		// For now we assume one object only.
		int numObjects = 1;
		std::vector<HitgroupRecord> hitgroupRecords;
		for (int i = 0; i < numObjects; i++) {
			int objectType = 0;
			HitgroupRecord rec;
			OPTIX_CHECK(optixSbtRecordPackHeader(_hitgroups[objectType], &rec));
			// Populate the geometry record.
			rec.data.index = (sibr::Vector3u*)_indices.devicePtr();
			rec.data.vertex = (sibr::Vector3f*)_vertices.devicePtr();
			rec.data.normal = (sibr::Vector3f*)_normals.devicePtr();
			rec.data.texcoord = (sibr::Vector2f*)_texcoords.devicePtr();
			rec.data.color = (sibr::Vector3f*)_colors.devicePtr();
			rec.data.material = (int*)_materials.devicePtr();
			hitgroupRecords.push_back(rec);
		}
		_hitgroupRecords.set(hitgroupRecords);
		_sbt.hitgroupRecordBase = _hitgroupRecords.devicePtr();
		_sbt.hitgroupRecordStrideInBytes = sizeof(HitgroupRecord);
		_sbt.hitgroupRecordCount = (int)hitgroupRecords.size();
	}

	void OptixRaycaster::updateCamera(const InputCamera& eye) {
		_launchParams->camera.position = eye.position();
		sibr::CameraRaycaster::computePixelDerivatives(eye, _launchParams->camera.dx, _launchParams->camera.dy, _launchParams->camera.upLeftOffset);
	}

	void OptixRaycaster::onRender(const sibr::InputCamera& eye) {
		if (!_launchParamsBuffer) {
			_launchParamsBuffer.alloc(sizeof(LaunchParams));
		}
		// Map the destination texture.
		CUDA_CHECK(cudaGraphicsMapResources(1, &_dst.rsc, _cudaStream));
		uint32_t* dstPtr;
		size_t dstSize;
		CUDA_CHECK(cudaGraphicsResourceGetMappedPointer((void**)&dstPtr, &dstSize, _dst.rsc));

		_launchParams->dst = dstPtr;
		// Update camera params.
		updateCamera(eye);

		_launchParamsBuffer.send(_launchParams.get(), 1);

		OPTIX_CHECK(optixLaunch(_pipeline, _cudaStream,
			_launchParamsBuffer.devicePtr(),
			_launchParamsBuffer.size(),
			&_sbt,
			/*! dimensions of the launch: */
			_launchParams->size.x(),
			_launchParams->size.y(),
			1
		));
		// Wait for operations to be done.
		cudaGraphicsUnmapResources(1, &_dst.rsc, _cudaStream);
		CUDA_SYNC_CHECK();

		// Copy from the PBO to the texture.
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _dst.rt->handle());
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, _dst.pbo);
		glTexImage2D(GL_TEXTURE_2D, 0, _dst.internalFormat, (GLsizei)_launchParams->size.x(), (GLsizei)_launchParams->size.y(), 0, _dst.format, _dst.type, (void*)0);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	}


	void OptixRaycaster::registerSource(const RenderTargetRGBA32F& dst, uint handle) {
		cudaGraphicsGLRegisterImage(&_srcRsc, dst.handle(handle), GL_TEXTURE_2D,
			cudaGraphicsMapFlagsReadOnly);
	}

	void OptixRaycaster::registerDirection(const RenderTargetRGBA32F& dir, uint handle) {
		cudaGraphicsGLRegisterImage(&_dirRsc, dir.handle(handle), GL_TEXTURE_2D,
			cudaGraphicsMapFlagsReadOnly);
	}




	void OptixRaycaster::onRender() {
		if (!_launchParamsBuffer) {
			_launchParamsBuffer.alloc(sizeof(LaunchParams));
		}

		// Map the source texture.
		{
			CUDA_CHECK(cudaGraphicsMapResources(1, &_srcRsc, _cudaStream));
			cudaArray_t texArray;
			CUDA_CHECK(cudaGraphicsSubResourceGetMappedArray(&texArray, _srcRsc, 0, 0));
			cudaResourceDesc res_desc;
			memset(&res_desc, 0, sizeof(res_desc));
			res_desc.resType = cudaResourceTypeArray;
			res_desc.res.array.array = texArray;
			cudaTextureDesc texDesc;
			memset(&texDesc, 0, sizeof(texDesc));
			texDesc.addressMode[0] = cudaAddressModeBorder;
			texDesc.addressMode[1] = cudaAddressModeBorder;
			texDesc.addressMode[2] = cudaAddressModeBorder;
			texDesc.filterMode = cudaFilterModePoint;
			texDesc.readMode = cudaReadModeElementType;
			CUDA_CHECK(cudaCreateTextureObject(&_launchParams->buffers.positions, &res_desc, &texDesc, nullptr));
		}
		// Map the direction texture.
		{
			CUDA_CHECK(cudaGraphicsMapResources(1, &_dirRsc, _cudaStream));
			cudaArray_t texArrayDir;
			CUDA_CHECK(cudaGraphicsSubResourceGetMappedArray(&texArrayDir, _dirRsc, 0, 0));
			cudaResourceDesc res_descDir;
			memset(&res_descDir, 0, sizeof(res_descDir));
			res_descDir.resType = cudaResourceTypeArray;
			res_descDir.res.array.array = texArrayDir;
			cudaTextureDesc texDesc;
			memset(&texDesc, 0, sizeof(texDesc));
			texDesc.addressMode[0] = cudaAddressModeBorder;
			texDesc.addressMode[1] = cudaAddressModeBorder;
			texDesc.addressMode[2] = cudaAddressModeBorder;
			texDesc.filterMode = cudaFilterModePoint;
			texDesc.readMode = cudaReadModeElementType;
			CUDA_CHECK(cudaCreateTextureObject(&_launchParams->buffers.directions, &res_descDir, &texDesc, nullptr));
		}

		// Map the destination texture.
		CUDA_CHECK(cudaGraphicsMapResources(1, &_dst.rsc, _cudaStream));
		uint32_t* dstPtr;
		size_t dstSize;
		CUDA_CHECK(cudaGraphicsResourceGetMappedPointer((void**)&dstPtr, &dstSize, _dst.rsc));

		_launchParams->dst = dstPtr;
		_launchParamsBuffer.send(_launchParams.get(), 1);

		OPTIX_CHECK(optixLaunch(_pipeline, _cudaStream,
			_launchParamsBuffer.devicePtr(),
			_launchParamsBuffer.size(),
			&_sbt,
			/*! dimensions of the launch: */
			_launchParams->size.x(),
			_launchParams->size.y(),
			1
		));
		// Wait for operations to be done.
		cudaGraphicsUnmapResources(1, &_dst.rsc, _cudaStream);
		cudaDestroyTextureObject(_launchParams->buffers.positions);
		cudaGraphicsUnmapResources(1, &_srcRsc, _cudaStream);
		cudaDestroyTextureObject(_launchParams->buffers.directions);
		cudaGraphicsUnmapResources(1, &_dirRsc, _cudaStream);
		CUDA_SYNC_CHECK();
		// Copy from the PBO to the texture.
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _dst.rt->handle());
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, _dst.pbo);
		glTexImage2D(GL_TEXTURE_2D, 0, _dst.internalFormat, (GLsizei)_launchParams->size.x(), (GLsizei)_launchParams->size.y(), 0, _dst.format, _dst.type, (void*)0);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	}

}