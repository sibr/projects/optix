/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include "Config.hpp"
#include "CUDABuffer.hpp"
#include "OptixSharedStructures.h"

#include <core/graphics/Mesh.hpp>
#include <core/graphics/MaterialMesh.hpp>
#include <core/graphics/RenderTarget.hpp>
#include <core/assets/InputCamera.hpp>
#include <core/system/Matrix.hpp>

#include <optix.h>
# pragma warning(push, 0)
#include <optix_stubs.h>
# pragma warning(pop)
#include <cuda_gl_interop.h>

namespace sibr
{
	/** Raycaster relying on Optix internally.
	 * This class supports two modes for raycasting:
	 * - you provide camera information and a ray is cast for each pixel, from the camera origin.
	 * - you provide two textures containing rays origin and direction.
	 * In both cases, you will have to register a destination rendertarget.
	 * You can provide your own ray generation and closest hit / any hit / miss shaders as a precompiled PTX CUDA program.
	 * For this,
	 * - create a .cu file similar to optix/apps/SIBR_optix_test_app/OptixShadersTest.cu and put it in your application/library.
	 * - in the associated CMakeLists.txt, add the following lines:
	 *		cuda_compile_and_embed(my_embedded_ptx_code path/to/your.cu)
	 *		source_group("Source Files\\kernels" FILES ${my_embedded_ptx_code})
	 *	 and when you are adding ${SOURCES} to your library/executable, add ${my_embedded_ptx_code}
	 * - when creating the raycaster, provide the shaders names and the PTX string.
	 * Based on Ingo Wald Optix 7 tutorial (https://gitlab.com/ingowald/optix7course).
	 */
	class SIBR_OPTIX_EXPORT OptixRaycaster
	{
	public:

		SIBR_DISALLOW_COPY(OptixRaycaster);
		SIBR_CLASS_PTR(OptixRaycaster);

		/// Constructor.
		OptixRaycaster();

		enum class SpawnMode {
			CAMERA, BUFFERS, BUFFERSPOS
		};
		/** Set the minimal intersection distance, useful to avoid self occlusions.
		 *\param minDist world space minimal intersection distance
		 */
		void setMinDist(float minDist);

		/** Optix program options. */
		struct Options {
			std::string entryName;
			std::string missName;
			std::string closestName;
			std::string anyName;
			std::string ptxCode;
			bool debug = false;
		};
		
		/** Create the raycasting programs based on PTX code and entrypoint names.
		 *\param options the entry points and compiled shader code
		 */
		void createPrograms(Options options);

		/** Create the scene from a mesh.
		 *\param mesh the mesh to raycast against
		 */
		void createScene(const Mesh & mesh);

		/** Untested for now, could cause an unwanted recursive call.
		 *\param mesh the mesh to raycast against
		 **/
		void createScene(const MaterialMesh & mesh);

		/** Bind the programs and geometry together. */
		void bindProgramsAndScene();

		/** CPU update only, upload will be performed at rendering time. */
		void updateCamera(const InputCamera& eye);

		/** Perform rendering using the camera as a rays source.
		 *\param eye the camera to cast rays from
		 */
		void onRender(const InputCamera & eye);

		/** Perform rendering, spawning rays ffrom the information
		 * contained in the source and direction maps.
		 */
		void onRender();

		void registerSource(const RenderTargetRGBA32F & dst, uint handle = 0);

		void registerDirection(const RenderTargetRGBA32F& dst, uint handle = 0);

		/** Only use with 1/2/4 channels and uchar/float format for now. */
		template <typename T_Type, unsigned T_NumComp>
		void registerDestination(std::shared_ptr<RenderTarget<T_Type, T_NumComp>> dst) {
			_dst.rt = dst;
			_launchParams->size[0] = dst->w();
			_launchParams->size[1] = dst->h();
			glGenBuffers(1, &_dst.pbo);
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, _dst.pbo);
			glBufferData(GL_PIXEL_UNPACK_BUFFER, dst->w() * dst->h() * T_NumComp * sizeof(T_Type), (void*)0, GL_DYNAMIC_COPY);
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
			cudaGraphicsGLRegisterBuffer(&_dst.rsc, _dst.pbo, cudaGraphicsRegisterFlagsWriteDiscard);

			_dst.internalFormat = GLFormat<T_Type, T_NumComp>::internal_format;
			_dst.format = GLFormat<T_Type, T_NumComp>::format;
			_dst.type = GLType<T_Type>::type;
		}

	private:

		void createPipeline();

		bool _init = false;

		CUcontext _cudaContext;
		CUstream _cudaStream;
		cudaDeviceProp _deviceProps;
		OptixDeviceContext _optixContext;

		OptixPipeline _pipeline;
		OptixPipelineCompileOptions _compileOptions;
		OptixPipelineLinkOptions _linkOptions;
		OptixModule _module;
		OptixModuleCompileOptions _moduleOptions;

		std::vector<OptixProgramGroup> _raygens;
		CUDABuffer _raygenRecords;
		std::vector<OptixProgramGroup> _misses;
		CUDABuffer _missRecords;
		std::vector<OptixProgramGroup> _hitgroups;
		CUDABuffer _hitgroupRecords;
		OptixShaderBindingTable _sbt = {};

		CUDABuffer _vertices, _normals, _texcoords, _colors, _materials;
		CUDABuffer _indices;
		CUDABuffer _ASBuffer;

		std::shared_ptr<LaunchParams> _launchParams;
		CUDABuffer _launchParamsBuffer;
		
		cudaGraphicsResource_t _srcRsc, _dirRsc;
		
		struct DstInfos {
			IRenderTarget::Ptr rt;
			cudaGraphicsResource_t rsc;
			GLuint pbo;
			GLenum format, internalFormat, type;
	};
		DstInfos _dst;
	};


} // namespace sibr

