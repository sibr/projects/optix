/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once
#include <stdint.h>
#include <core/system/Vector.hpp>
namespace sibr
{
	struct LaunchParams
	{
		uint32_t *dst;
		sibr::Vector2i size = { 0, 0 };
		float minDist = 0.001f;
		
		struct {
			sibr::Vector3f position;
			sibr::Vector3f upLeftOffset;
			sibr::Vector3f dx;
			sibr::Vector3f dy;
		} camera;
		
		struct {
			cudaTextureObject_t positions;
			cudaTextureObject_t directions;
		} buffers;

		OptixTraversableHandle traversable;

		//virtual ~LaunchParams();
	};
	
	struct TriangleMeshData {
		sibr::Vector3f *vertex;
		sibr::Vector3f *normal;
		sibr::Vector2f *texcoord;
		sibr::Vector3f *color;
		int * material;
		sibr::Vector3u *index;
		//bool hasTexture;
		//cudaTextureObject_t texture;
	};


	/* Common parameters */
	extern "C" __constant__ LaunchParams optixLaunchParams;

	enum { SURFACE_RAY_TYPE = 0, RAY_TYPE_COUNT };

	static __forceinline__ __device__
		void* unpackPointer(uint32_t i0, uint32_t i1) {
		const uint64_t uptr = static_cast<uint64_t>(i0) << 32 | i1;
		void* ptr = reinterpret_cast<void*>(uptr);
		return ptr;
	}

	static __forceinline__ __device__
		void  packPointer(void* ptr, uint32_t& i0, uint32_t& i1) {
		const uint64_t uptr = reinterpret_cast<uint64_t>(ptr);
		i0 = uptr >> 32;
		i1 = uptr & 0x00000000ffffffff;
	}

	template<typename T>
	static __forceinline__ __device__ T* getPRD() {
		const uint32_t u0 = optixGetPayload_0();
		const uint32_t u1 = optixGetPayload_1();
		return reinterpret_cast<T*>(unpackPointer(u0, u1));
	}

} // namespace sibr

